import { defineStore } from "pinia";

export const userDetailsStore = defineStore("user", {
  state: () => ({
    token: null,
    products: [],
  }),

  getters: {
    accessToken(state) {
      return state.token;
    },
  },

  actions: {
    setToken(token) {
      this.token = token;
    },
  },
});

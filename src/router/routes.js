const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      {
        path: "/login",
        component: () => import("pages/Login.vue"),
      },
      {
        path: "/register",
        component: () => import("pages/Register.vue"),
      },
      {
        path: "/orders",
        component: () => import("pages/Orders.vue"),
      },
      {
        path: "/message",
        component: () => import("pages/Message.vue"),
      },
      {
        path: "/productmessage",
        component: () => import("pages/Productmessage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;

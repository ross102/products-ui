import axios from "axios";

// we can also put in an env varibale
// (process.env.xxxx) but we can use this for the demo

export default axios.create({
  baseURL: "https://products-api-test-com.onrender.com/",
  timeout: 60000,
});
